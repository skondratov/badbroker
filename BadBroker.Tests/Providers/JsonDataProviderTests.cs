﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BadBroker.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BadBroker.Tests.Providers
{
    [TestClass]
    public class JsonDataProviderTest
    {
        [TestMethod]
        public async Task JsonProviderWrogUrl()
        {
            var cancelationCtx = new CancellationTokenSource();
            using (var jsonProvider = new JsonDataProvider(new Uri("http://fake.com"), cancelationCtx.Token))
            {
                try
                {
                    string result = await jsonProvider.RequestStringAsync("fake");
                }
                catch (HttpRequestException)
                {
                    return;
                }
                catch (Exception)
                {
                    Assert.Fail("Wrong exception when try to request fake URL");
                }

                Assert.Fail("There is no exception when try to request fake URL");
            }
        }

        [TestMethod]
        public async Task JsonProviderTest()
        {
            const string uri = "http://api.openweathermap.org/";
            try
            {
                var cancelationCtx = new CancellationTokenSource();
                const int countOfSimultaneousRequests = 30;
                using (var jsonProvider = new JsonDataProvider(new Uri(uri), cancelationCtx.Token))
                {
                    Func<int, string> prepareRequestString =
                        x => String.Format("data/2.5/weather?lat={0}&lon=139&appid=0b435c3df407e5ac34a1ac20b65a3482", x);

                    var tasks =
                        Enumerable.Range(1, countOfSimultaneousRequests)
                            .Select(i => jsonProvider.RequestStringAsync(prepareRequestString(i)))
                            .ToList();

                    var ret = new List<string>();
                    ret.AddRange(await Task.WhenAll(tasks).ConfigureAwait(false));

                    Assert.AreEqual(countOfSimultaneousRequests, ret.Count, "Wrong simultaneous requests result");
                }
            }
            catch (HttpRequestException)
            {
                Assert.Fail(String.Format("Error while request to {0}", uri));
            }
        }
    }
}
