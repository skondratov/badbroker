﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BadBroker.Models;
using BadBroker.Providers;
using BadBroker.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BadBroker.Tests.Providers
{
    [TestClass]
    public class FixerIoDataProviderTests
    {
        public class FakeLocalRatesCache : ILocalRatesCache
        {
            public FakeLocalRatesCache()
            {
                DailyCurrencyRates = new AlwaysEmptyDbSet<CurrencyRates>();
            }

            public IDbSet<CurrencyRates> DailyCurrencyRates { get; private set; }

            public int SaveChanges()
            {
                return 0;
            }

            public void Dispose()
            {
                // No need to Dispose and use withing 'using' block
            }
        }

        public class FakeLocalRatesCacheProvider : LocalRatesCacheProvider
        {
            public override ILocalRatesCache GetLocalRatesCache()
            {
                return new FakeLocalRatesCache();
            }
        }

        [TestMethod]
        public async Task FixerIoDataProviderTests_CheckArgumentExceptions()
        {
            try
            {
                var cancelationCtx = new CancellationTokenSource();
                using (var fixerIoDataProvider = new FixerIoDataProvider(cancelationCtx.Token))
                {
                    var ret = fixerIoDataProvider.RequestRatesOnTheTimePeriod(
                        new DateTime(2012, 12, 20),
                        new DateTime(2012, 11, 20)); // Date from > date to
                    await Task.WhenAll(ret).ConfigureAwait(false);

                    Assert.Fail("Wrong argument is allowed");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }

            try
            {
                var cancelationCtx = new CancellationTokenSource();
                using (var fixerIoDataProvider = new FixerIoDataProvider(cancelationCtx.Token))
                {
                    var ret = fixerIoDataProvider.RequestRatesOnTheTimePeriod(
                        new DateTime(2012, 12, 20),
                        new DateTime(2016, 11, 20)); // long period
                    await Task.WhenAll(ret).ConfigureAwait(false);
                    Assert.Fail("Wrong argument is allowed");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        [TestMethod]
        public async Task FixerIoDataProviderTestFakeDb()
        {
            try
            {
                var cancelationCtx = new CancellationTokenSource();
                using (var fixerIoDataProvider =
                    new FixerIoDataProvider(
                        cancelationCtx.Token,
                        new FakeLocalRatesCacheProvider()))
                {
                    var from = new DateTime(2012, 11, 20); // Good time for RUB ;(
                    var to = new DateTime(2012, 12, 20);
                    int daysCount = (to - from).Days + 1;

                    var tasks = fixerIoDataProvider.RequestRatesOnTheTimePeriod(from, to);

                    var ret = new List<CurrencyRates>();
                    ret.AddRange(await Task.WhenAll(tasks).ConfigureAwait(false));

                    Assert.AreEqual(daysCount, ret.Count, "Wrong simultaneous requests result");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Assert.Fail("Unexpected wrong argument exception");
            }
            catch (HttpRequestException)
            {
                Assert.Fail("Error while request to fixer.io");
            }
        }
    }
}
