﻿using System;
using System.Collections.Generic;
using BadBroker.Logic;
using BadBroker.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BadBroker.Tests.Logic
{
    [TestClass]
    public class DefaultBrokerTest
    {
        [TestMethod]
        public void DefaultBrokerTest_BestRevenueTest()
        {
            // I know, its not brilliant way for deriving test data, but time is money:)
            var rates = new List<CurrencyRates>();
            rates.Add(new CurrencyRates
            {
                Date = new DateTime(2012, 01, 01),
                EUR = 1,
                GBP = 2,
                JPY = 2,
                RUB = 1
            });

            rates.Add(new CurrencyRates
            {
                Date = new DateTime(2012, 01, 02),
                EUR = 55,
                GBP = 23,
                JPY = 51,
                RUB = 120
            });

            rates.Add(new CurrencyRates
            {
                Date = new DateTime(2012, 01, 03),
                EUR = 12,
                GBP = 22,
                JPY = 34,
                RUB = 80
            });

            rates.Add(new CurrencyRates
            {
                Date = new DateTime(2012, 01, 04),
                EUR = 32,
                GBP = 33,
                JPY = 21,
                RUB = 10
            });

            var broker = new DefaultBroker(dayFee: 2);
            Position bestPosition = broker.GetBestRevenue(rates, amountOfMoney: 1600);
            Assert.AreEqual(bestPosition.Start, new DateTime(2012, 01, 02), "Wrong BestRevenue results");
            Assert.AreEqual(bestPosition.Finish, new DateTime(2012, 01, 04), "Wrong BestRevenue results");
            Assert.AreEqual(bestPosition.Currency, "RUB", "Wrong BestRevenue results");
            Assert.AreEqual(bestPosition.RevenueInUsd, 19196, "Wrong BestRevenue results");
        }
    }
}
