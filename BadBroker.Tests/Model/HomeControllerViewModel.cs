﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BadBroker.Models;



namespace BadBroker.Tests.Model
{
    [TestClass]
    public class HomeControllerViewModelTest
    {
        [TestMethod]
        public void ValidationTest()
        {
            {
                var model = new HomeControllerViewModel();
                model.DateFrom = new DateTime(2012, 01, 01);
                model.DateTo = new DateTime(2012, 01, 21);
                model.AmountOfMoney = 1;

                var result = Validator.TryValidateObject(model, new ValidationContext(model, null, null), null, true);

                Assert.IsTrue(result, "Valid data marked as invalid");
            }

            {
                var model = new HomeControllerViewModel();
                model.DateFrom = new DateTime(2012, 01, 01);
                model.DateTo = new DateTime(2012, 01, 01); // Dates euqal
                model.AmountOfMoney = 1;

                var result = Validator.TryValidateObject(model, new ValidationContext(model, null, null), null, true);

                Assert.IsFalse(result, "Equal dates are allowed");
            }

           {
                var model = new HomeControllerViewModel();
                model.DateFrom = new DateTime(2012, 01, 01);
                model.DateTo = new DateTime(2011, 01, 01); // Date less
                model.AmountOfMoney = 1;

                var result = Validator.TryValidateObject(model, new ValidationContext(model, null, null), null, true);

                Assert.IsFalse(result, "DateFrom > DateTo");
            }

            {
                var model = new HomeControllerViewModel();
                model.DateFrom = new DateTime(2012, 01, 01);
                model.DateTo = new DateTime(2013, 01, 01); // huge period
                model.AmountOfMoney = 1;

                var result = Validator.TryValidateObject(model, new ValidationContext(model, null, null), null, true);

                Assert.IsFalse(result, "Period > 2 months");
            }

            {
                var model = new HomeControllerViewModel();
                model.DateFrom = new DateTime(2012, 01, 01);
                model.DateTo = DateTime.Now.Date; // Date in future
                model.AmountOfMoney = 1;

                var result = Validator.TryValidateObject(model, new ValidationContext(model, null, null), null, true);

                Assert.IsFalse(result, "Period in future");
            }
        }
    }
}