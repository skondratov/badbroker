﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace BadBroker.Providers
{
    /// <summary>
    /// Data provider for JSON data
    /// Requests data using HttpClient
    /// Makes by default 3 attempts with 5000ms for each
    /// If all attempts - fails throws HttpRequestException
    /// </summary>
    public class JsonDataProvider : IDisposable
    {
        private readonly HttpClient _httpClient;
        private readonly CancellationToken _cancellationToken;

        public JsonDataProvider(
            Uri serviceUri,
            CancellationToken cancellationToken)
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = serviceUri;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            _cancellationToken = cancellationToken;

            // Default settings
            _httpClient.Timeout = TimeSpan.FromMilliseconds(5000);
            RequestAttemptsInCaseOfTimeout = 3;
        }

        public Uri ServiceUri
        {
            get { return _httpClient.BaseAddress; }
        }

        public TimeSpan RequestTimeout
        {
            get { return _httpClient.Timeout; }
            set { _httpClient.Timeout = value; }
        }

        public int RequestAttemptsInCaseOfTimeout { get; set; }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        /// <summary>
        /// Request item of concrete type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestString"></param>
        /// <returns></returns>
        public async Task<T> RequestItemAsync<T>(string requestString)
        {
            using (var response = await RequestAsync(requestString).ConfigureAwait(false))
            {
                return await response.Content.ReadAsAsync<T>().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Send request and return JSON string as result
        /// </summary>
        /// <param name="requestString"></param>
        /// <returns></returns>
        public async Task<string> RequestStringAsync(string requestString)
        {
            using (var response = await RequestAsync(requestString).ConfigureAwait(false))
            {
                return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Implementation function, logic described in class constructor
        /// </summary>
        /// <param name="requestString"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> RequestAsync(string requestString)
        {
            foreach (var attempt in Enumerable.Range(1, RequestAttemptsInCaseOfTimeout))
            {
                HttpResponseMessage ret = null;
                try
                {
                    ret = await _httpClient.GetAsync(requestString, _cancellationToken).ConfigureAwait(false);
                    if (ret.IsSuccessStatusCode)
                        return ret;
                }
                catch (TaskCanceledException e)
                {
                    if (ret != null)
                    {
                        ret.Dispose();
                    }
                    if (e.CancellationToken.IsCancellationRequested)
                        throw;
                }
            }

            throw new HttpRequestException(
                string.Format(
                    "Unable to pefrom request {0} (Timout: {1}, Attempts: {2})",
                    ServiceUri + requestString,
                    RequestTimeout,
                    RequestAttemptsInCaseOfTimeout));
        }
    }
}