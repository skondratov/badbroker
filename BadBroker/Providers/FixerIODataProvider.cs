﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BadBroker.Models;

namespace BadBroker.Providers
{
    /// <summary>
    /// Data provider for fixer.io JSON service improved with local cache
    /// </summary>
    public class FixerIoDataProvider : IDisposable
    {
        private readonly JsonDataProvider _jsonDataProvider;
        private readonly LocalRatesCacheProvider _localRatesCacheProvider;

        /// <summary>
        /// If ratesCache is null it will be created locally and disposed together with current instance
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="localRatesCacheProvider"></param>
        public FixerIoDataProvider(
            CancellationToken cancellationToken,
            LocalRatesCacheProvider localRatesCacheProvider = null)
        {
            _jsonDataProvider =
                new JsonDataProvider(
                    new Uri("http://api.fixer.io/"),
                    cancellationToken);
            _localRatesCacheProvider = localRatesCacheProvider ?? new LocalRatesCacheProvider();
        }

        public void Dispose()
        {
            _jsonDataProvider.Dispose();
        }

        /// <summary>
        /// Prepare 'set' of tasks which will load currency rates on specified dates
        /// If some rates are cached it will return task which loads item from cache
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public IEnumerable<Task<CurrencyRates>> RequestRatesOnTheTimePeriod(
            DateTime from,
            DateTime to)
        {
            // Truncate
            var fromDate = from.Date;
            var toDate = to.Date;

            // Not a validation just a common sense
            if (fromDate > toDate)
            {
                throw new ArgumentOutOfRangeException("'From' date must not must not be greather than 'To' date");
            }

            // Not a validation just a common sense
            const int maximumAllowedInteval = 70; // It actually means 70 request is possible, but 71 is not
            if (toDate.Subtract(fromDate).TotalDays > maximumAllowedInteval)
            {
                throw new ArgumentOutOfRangeException("Maximum time period must not exceed 70 days");
            }

            using (var ratesCache = _localRatesCacheProvider.GetLocalRatesCache())
            {
                var query = from elem in ratesCache.DailyCurrencyRates
                    where elem.Date >= fromDate && elem.Date <= toDate
                    orderby elem.Date
                    select elem;
                using (var cachedObject = query.GetEnumerator())
                {
                    cachedObject.MoveNext();
                    foreach (var date in DateRange(fromDate, toDate))
                    {
                        if (cachedObject.Current != null && cachedObject.Current.Date == date)
                        {
                            yield return Task.FromResult(cachedObject.Current);
                            cachedObject.MoveNext();
                        }
                        else
                        {
                            yield return RequestRatesRatesFromWebAsync(date);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Request item from web. It is assumed that this item is not available in cache.
        /// After item retrieval it will make attempt to save item in cache
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private async Task<CurrencyRates> RequestRatesRatesFromWebAsync(DateTime date)
        {
            var currencyRatesInFixerIoFormat =
                await
                    _jsonDataProvider.RequestItemAsync<CurrencyRatesInFixerIoFormat>(PrepareRequestString(date))
                        .ConfigureAwait(false);

            // At weekends and holidays rates date is took from last working day (as well as all rates values)
            // Threre is no requirements how to handle these days, does broker take fee for weekends and holidays?
            // Do we need close position before weekend? Thus, consider holidays and weekend as usual days. Preseting Date value from request.
            currencyRatesInFixerIoFormat.date = date;

            currencyRatesInFixerIoFormat.rates.Date = currencyRatesInFixerIoFormat.date;
            using (var ratesCache = _localRatesCacheProvider.GetLocalRatesCache())
            {
                try
                {
                    ratesCache.DailyCurrencyRates.Add(currencyRatesInFixerIoFormat.rates);
                    ratesCache.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    // Skip Duplicate key found exception to avoid problems with concurrency
                    var innerException = ex.InnerException as UpdateException;
                    if (innerException == null)
                        throw;
                    var sqlInnerException = innerException.InnerException as SqlException;
                    if (sqlInnerException == null)
                        throw;

                    const int duplicateKeyConstraintErrorCode = 2627;
                    if (sqlInnerException.Number != duplicateKeyConstraintErrorCode)
                        throw;
                }
            }
            return currencyRatesInFixerIoFormat.rates;
        }

        /// <summary>
        /// Helper function. Returns IEnumerable to dates from [from, to] interval
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private static IEnumerable<DateTime> DateRange(
            DateTime @from,
            DateTime to)
        {
            return Enumerable.Range(0, to.Subtract(@from).Days + 1)
                .Select(d => @from.AddDays(d));
        }

        /// <summary>
        /// Helper function builds request string for fixer.io service
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static string PrepareRequestString(DateTime date)
        {
            const string dateFormat = "yyyy-MM-dd";
            const string baseCurrency = "USD";

            var usedCurencies = string.Join(",", CurrencyRates.UsedCurencies());
            return string.Format(
                "{0}/?base={1}&symbols={2}",
                date.ToString(dateFormat),
                baseCurrency,
                usedCurencies);
        }

        /// <summary>
        /// Helper class which is used to parse fixer.io JSON result
        /// </summary>
        private class CurrencyRatesInFixerIoFormat
        {
            public string @base { get; set; }
            public DateTime date { get; set; }
            public CurrencyRates rates { get; set; }
        }
    }
}