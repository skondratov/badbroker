﻿using System;
using System.Data.Entity;
using BadBroker.Models;

namespace BadBroker.Providers
{
    /// <summary>
    /// Provider class for LocalRatesCache.
    /// Required for dependency injection in FixerIoDataProvider which allows to write FakeDb tests
    /// </summary>
    public class LocalRatesCacheProvider
    {
        public virtual ILocalRatesCache GetLocalRatesCache()
        {
            return new LocalRatesCache();
        }
    }

    /// <summary>
    /// Required for dependency injection in FixerIoDataProvider which allows to write FakeDb tests
    /// </summary>
    public interface ILocalRatesCache : IDisposable
    {
        IDbSet<CurrencyRates> DailyCurrencyRates { get; }
        int SaveChanges();
    }

    /// <summary>
    /// Use EF Code first approach for RatesCache
    /// </summary>
    public class LocalRatesCache : DbContext, ILocalRatesCache
    {
        public IDbSet<CurrencyRates> DailyCurrencyRates { get; set; }
    }
}