﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BadBroker.Models
{
    /// <summary>
    /// Helper structure, encapsulates logic of calculating best ratio between two CurrencyRates
    /// It was placed near to CurrencyRates class Since it has hardcoded Currency names
    /// </summary>
    public struct BestRatio
    {
        /// <summary>
        /// Constructor finds best ratio for each pair of from.Cur / to.Cur
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public BestRatio(
            CurrencyRates from,
            CurrencyRates to) : this()
        {
            BestRatio tmpThis = this;
            Action<decimal, string> solver = (value, name) =>
            {
                if (value > tmpThis.Ratio)
                {
                    tmpThis.Ratio = value;
                    tmpThis.CurrencyName = name;
                }
            };

            solver(from.RUB/to.RUB, "RUB");
            solver(from.EUR/to.EUR, "EUR");
            solver(from.GBP/to.GBP, "GBP");
            solver(from.JPY/to.JPY, "JPY");
            this = tmpThis;
        }

        public decimal Ratio { get; private set; }
        public string CurrencyName { get; private set; }
    }

    /// <summary>
    /// Currency rates for a day. This entity reflects database table structure
    /// and partly reflects fixer.io rates format
    /// </summary>
    public class CurrencyRates
    {
        [Key]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime Date { get; set; }

        public decimal RUB { get; set; }
        public decimal EUR { get; set; }
        public decimal GBP { get; set; }
        public decimal JPY { get; set; }

        /// <summary>
        /// Use reflection to get all non-key attribute. Fortunately they all are currency names
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> UsedCurencies()
        {
            return 
                from prop in typeof (CurrencyRates).GetProperties()
                where prop.GetCustomAttributes(typeof (KeyAttribute), false).Length == 0
                select prop.Name;
        }
    }
}