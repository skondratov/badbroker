﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BadBroker.Models
{
    /// <summary>
    /// Part of model which is attached to HomeControllerViewModel when form is posted
    /// </summary>
    public class HomeControllerResultViewModel
    {
        public HomeControllerResultViewModel()
        {
            Rates = new List<CurrencyRates>();
            ErrorMessage = string.Empty;
        }

        /// <summary>
        /// Used to build BestRevenue result message
        /// </summary>
        public Position BestPosition { get; set; }

        /// <summary>
        /// Used to build table of rates
        /// </summary>
        [UIHint("CurrencyRatesList")]
        public List<CurrencyRates> Rates { get; set; }

        /// <summary>
        /// If this string is not empty whole result structure is ignored in favor of showing this message
        /// </summary>
        public string ErrorMessage { get; set; }
    }

    /// <summary>
    /// Quite heavy class which sends all required data to all BestRevenue Views
    /// I do not proud of this design solution, but it allows to support JS / non-Js modes with quite small amount of code
    /// </summary>
    [Bind(Include = "DateFrom,DateTo,AmountOfMoney")]
    public class HomeControllerViewModel : IValidatableObject
    {
        [Display(Name = "From")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "From is required")]
        public DateTime DateFrom { get; set; }

        [Display(Name = "To")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "To is required")]
        public DateTime DateTo { get; set; }

        [Display(Name = "Amount of money")]
        [DataType(DataType.Currency)]
        [Range(0.01, double.MaxValue, ErrorMessage = "Wrong 'Amount of money' value")]
        [Required(ErrorMessage = "'Amount of money' is required")]
        public decimal AmountOfMoney { get; set; }

        /// <summary>
        /// This field is null until form is posted
        /// </summary>
        public HomeControllerResultViewModel Result { get; set; }

        /// <summary>
        /// Server side validation
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DateFrom >= DateTo)
            {
                yield return new ValidationResult(
                    "'To' date must be greater than 'From' date");
            }

            const int allowedDateTimeRageInMonths = 2;
            var from = DateFrom;
            if (from.AddMonths(allowedDateTimeRageInMonths) < DateTo)
            {
                yield return new ValidationResult(
                    "Historical period cannot exceed 2 months.");
            }

            // From fixer.io: The rates are updated daily around 3PM CET.
            // I will consider UTC time for simplicity
            var lastUpdate = DateTime.UtcNow.AddHours(-15); // 3PM = 15
            if (DateTo > lastUpdate)
            {
                yield return new ValidationResult(
                    "Dates in future are not allowed. The rates are updated daily around 3PM CET. ");
            }
        }
    }
}