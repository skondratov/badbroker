﻿using System;

namespace BadBroker.Models
{
    /// <summary>
    /// Simple class, helps to represent Best Revenue function results
    /// </summary>
    public class Position
    {
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public string Currency { get; set; }
        public decimal RevenueInUsd { get; set; }
    }
}