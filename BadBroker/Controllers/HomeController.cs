﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using BadBroker.Logic;
using BadBroker.Models;
using BadBroker.Providers;
using System;

namespace BadBroker.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Main index controller loads BestRevenue form
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Contact page
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            return View();
        }

        /// <summary>
        /// Main controller for Best Revenue form. Works with or without javascript.
        /// </summary>
        /// <param name="model">Quite awkward objects with all required data for form and for result</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(
            HomeControllerViewModel model,
            CancellationToken cancellationToken)
        {
            model.Result = new HomeControllerResultViewModel();

            if (!ModelState.IsValid) 
                return CreateView(model);
            try
            {
                using (var fixerIo = new FixerIoDataProvider(cancellationToken))
                {
                    try
                    {
                        var ratesOnTheTimePeriodTask =
                            fixerIo.RequestRatesOnTheTimePeriod(model.DateFrom, model.DateTo);

                        model.Result.Rates.AddRange(await Task.WhenAll(ratesOnTheTimePeriodTask).ConfigureAwait(false));

                        var broker = new DefaultBroker(dayFee: 1);
                        model.Result.BestPosition = broker.GetBestRevenue(model.Result.Rates, model.AmountOfMoney);
                    }
                    catch (HttpRequestException)
                    {
                        model.Result.ErrorMessage =
                            "Error while request currency rates from external provider. Try repeat your request later.";
                    }
                }
            }
            catch (Exception)
            {
                if (!HandleError(model))
                {
                    // non-ajax calls will be redirected to error page
                    throw;
                }
            }
            return CreateView(model);
        }

        /// <summary>
        /// Create view regarding to request type. Send partial view in case of ajax request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ActionResult CreateView(HomeControllerViewModel model)
        {
            // @@ I don't like this(
            if (Request.IsAjaxRequest())
            {
                return PartialView("BestRevenueResults", model.Result);
            }
            return View("Index", model);
        }

        /// <summary>
        /// Handle unexpected error during Index controller execution. If form has been posted without Ajax, return false
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool HandleError(HomeControllerViewModel model)
        {
            if (!Request.IsAjaxRequest())
                return false;

            model.Result.ErrorMessage =
                "Something bad happened while Best Revenue calcualting. Please contact administrator";
            return true;
        }
    }
}