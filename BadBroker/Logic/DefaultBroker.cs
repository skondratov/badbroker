﻿using System;
using System.Collections.Generic;
using System.Linq;
using BadBroker.Models;

namespace BadBroker.Logic
{
    public class DefaultBroker
    {
        private readonly decimal _dayFee;

        public DefaultBroker(decimal dayFee)
        {
            if (dayFee <= 0)
                throw new ArgumentOutOfRangeException("dayFee");
            _dayFee = dayFee;
        }

        /// <summary>
        /// Calculates Best Revenue for given rates and amountOfMoney (difficulty O(n^2))
        /// </summary>
        /// <param name="rates"></param>
        /// <param name="amountOfMoney"></param>
        /// <returns></returns>
        public Position GetBestRevenue(
            List<CurrencyRates> rates,
            decimal amountOfMoney)
        {
            if (rates == null)
                throw new ArgumentNullException("rates");

            if (amountOfMoney <= 0)
                throw new ArgumentOutOfRangeException("amountOfMoney");

            // Ensure list is sorted, not much memory overhead: it is list of references
            rates = rates.OrderBy(x => x.Date).ToList();
            var daysCount = rates.Count;

            if (daysCount < 2)
                throw new ArgumentException("Can't calculate best best revenue for period < 2 days");

            Position bestPosition = null;
            for (var i = 0; i < daysCount - 1; ++i)
            {
                var from = rates[i];
                for (var j = i + 1; j < daysCount; ++j)
                {
                    var to = rates[j];
                    var ratio = new BestRatio(from, to);
                    var curRevenue = ratio.Ratio*amountOfMoney - _dayFee*(j - i);
                    if (bestPosition == null ||
                        bestPosition.RevenueInUsd < curRevenue)
                    {
                        bestPosition = new Position
                        {
                            Start = from.Date,
                            Finish = to.Date,
                            Currency = ratio.CurrencyName,
                            RevenueInUsd = Math.Round(curRevenue, 2)
                        };
                    }
                }
            }
            return bestPosition;
        }
    }
}